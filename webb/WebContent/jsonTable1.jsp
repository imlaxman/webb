<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<script type="text/javascript">
              $('#button').click(function() {
                    alert("submit handler has fired");
                    $.ajax({
                        type: 'GET',
                        url: "JsonServlet",  
                        data: $('#cityDetails').serialize(),

                        success: function(data){ 
                            $.each(data, function( index, value ) {
                               var row = $("<tr><td>" + value.firstName + "</td><td>" + value.lastName + "</td></tr>");
                               $("#myData").append(row);
                            });
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            alert('error: ' + textStatus + ': ' + errorThrown);
                        }
                    });
                    return false;//suppress natural form submission
                }); 

   </script>
   
   
   
<script type="text/javascript">    

    function returnJSONData()
    {
        var processData = 'JSON'; 
        alert("submit handler has fired");
          $.ajax({  
            type: "GET",  
            url: "JsonServlet",  
            data: $('#cityDetails').serialize(),
            success: function(result){
            	alert(result.firstName);
            	var obj = result;
            	Object.keys(obj).forEach(function(key) {
            	    var row = $("<tr><td>" + key + "</td><td>" + obj[key] + "</td></tr>");
                    $("#myData").append(row);
            	});
            	
            	/* $.each(result, function(index, value ) {
                    var row = $("<tr><td>" + value.firstName + "</td><td>" + value.lastName + "</td></tr>");
                    $("#myData").append(row);
                 }); */
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error status code: "+xhr.status);
                alert("Error details: "+ thrownError);
            }
          }); 

    }
        

</script>


</head>
<body>



<!-- <input id="button" type="Submit" value="JSON data from servlet"> <br> -->
<input type="Submit" value="JSON data from servlet func" onclick="returnJSONData();">


<table id="myData">

          </table>


</body>
</html>
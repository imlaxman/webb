<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<script type="text/javascript">    

    function returnJSONData()
    {
        var processData = 'JSON'; 
          $.ajax({  
            type: "GET",  
            url: "JsonServlet",  
            data: "processData="+processData,  
            success: function(data){
            	  var header=	("<tr><th>Id</th><th>First Name</th><th>Last Name</th></tr>");
                  $("#myData").append(header);
                  	for (j in data)  	{
                  		 var row = $("<tr><td>" + data[j].id + "</td><td>" + data[j].firstName + "</td><td>" + data[j].lastName + "</td></tr>");
                           $("#myData").append(row);
                    	}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Error status code: "+xhr.status);
                alert("Error details: "+ thrownError);
            }
          }); 

    }
        

</script>


<style type="text/css">
table {
    display: table;
    border-collapse: separate;
    border-spacing: 2px;
    border-color: grey;
    border:1px solid #ddd; 
}

td, th {
    padding:5px 10px !important;
     border:1px solid #ddd; 
}



</style>


</head>
<body>


 <br>  <br>  <br> 
 
 <div class="container">
 
 
<input type="Submit" value="JSON data from servlet" onclick="returnJSONData();"> <br> 

 <br> 
<table id="myData">

          </table>
</div>

</body>
</html>


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class JsonServlet
 */
public class JsonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JsonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request,
     * HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, 
        HttpServletResponse response) throws ServletException, IOException {
    	System.out.println("****INSIDE doGet()****");
    	try {
    		JsonArray jsonArray = new JsonArray();
    		JsonObject obj1 = new JsonObject();
    		JsonObject obj2 = new JsonObject();
    		JsonObject obj3 = new JsonObject();
    		JsonObject obj4 = new JsonObject();
    		
    		obj1.addProperty("id", "101");
    		obj1.addProperty("firstName", "Rama");
    		obj1.addProperty("lastName", "Ram");
    		obj1.addProperty("email", "ram@gmail.com");
    		obj1.addProperty("mobile", "9885098850");
    		
    		obj2.addProperty("id", "102");
    		obj2.addProperty("firstName", "Kiran");
    		obj2.addProperty("lastName", "kanaya");
    		obj2.addProperty("email", "kiran@gmail.com");
    		obj2.addProperty("mobile", "9885098851");
    		
    		obj3.addProperty("id", "103");
    		obj3.addProperty("firstName", "Ramesh");
    		obj3.addProperty("lastName", "Shyam");
    		obj3.addProperty("email", "ramesh");
    		obj3.addProperty("mobile", "9885098852");
    		
    		obj4.addProperty("id", "104");
    		obj4.addProperty("firstName", "Vignesh");
    		obj4.addProperty("lastName", "vignan");
    		obj4.addProperty("email", "vignesh@gmail.com");
    		obj4.addProperty("mobile", "9885098853");
    		
    		
    		
    		jsonArray.add(obj1);
    		jsonArray.add(obj2);
    		jsonArray.add(obj3);
    		jsonArray.add(obj4);
    		
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.println(jsonArray.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, 
     * HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, 
        HttpServletResponse response) 
            throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

}

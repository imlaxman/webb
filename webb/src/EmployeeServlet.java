

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EmployeeServlet
 */
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("****INSIDE doGet()");
		// defining the PrintWriter object
				PrintWriter out = response.getWriter();
				
				// setting the response type
				response.setContentType("application/json");
				
				// creating employee object
				Employee employee = new Employee();
				
				// setting the attributes
				employee.setEmpId(101);
				employee.setEmpName("Dinesh Krishnan");
				employee.setEmpAge(25);
				employee.setEmpQualifcation(new String[]{"MS", "MBA"});
				employee.setEmpEmailId("dinesh@dineshkrish.com");
				employee.setEmpPhone("+91 8989898989");
				
				// converting object to json using Gson api.
				out.println(JSONConverter.convert(employee));
				
				out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
